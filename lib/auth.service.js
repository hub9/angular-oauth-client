import { Observable, Subject, EMPTY, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformServer } from '@angular/common';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
var AuthServiceConfig = (function () {
    function AuthServiceConfig() {
    }
    return AuthServiceConfig;
}());
export { AuthServiceConfig };
var AuthService = (function () {
    function AuthService(config, http, platformId) {
        this.config = config;
        this.http = http;
        this.platformId = platformId;
        this.isAuthenticated = false;
        this.token = null;
        this.me = {};
        this.authData = {};
        this.authenticatedChanged = new Subject();
        if (isPlatformServer(this.platformId)) {
            return;
        }
        var t = window.localStorage.getItem('auth_data');
        if (t != null) {
            this.authData = JSON.parse(t);
            this.token = this.authData.access_token;
            this.isAuthenticated = true;
            this.authenticatedChanged.next(true);
            if (this.authData.expiration <= new Date().getTime() + 100000) {
                this.refresh_token().subscribe();
            }
        }
    }
    AuthService.prototype.login = function (username, password) {
        var _this = this;
        var data = 'username=' + username + '&password=' + password + '&grant_type=password&client_id=' +
            this.config.apiId + '&client_secret=' + this.config.apiSecret;
        var headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        var r = this.http.post(this.config.apiOauthUrl + 'token/', data, { headers: headers });
        return r.pipe(map(function (res) {
            _this.authData = res;
            _this.token = _this.authData.access_token;
            _this.isAuthenticated = true;
            _this.authenticatedChanged.next(true);
            _this.authData.expiration = new Date().getTime() + (_this.authData.expires_in * 1000);
            window.localStorage.setItem('auth_data', JSON.stringify(_this.authData));
            return _this.authData;
        }));
    };
    AuthService.prototype.logout = function () {
        this.token = null;
        this.isAuthenticated = false;
        this.authenticatedChanged.next(false);
        this.authData = {};
        if (!isPlatformServer(this.platformId)) {
            window.localStorage.removeItem('auth_data');
        }
    };
    AuthService.prototype.refresh_token = function () {
        var _this = this;
        var data = 'grant_type=refresh_token&client_id=' + this.config.apiId + '&client_secret=' +
            this.config.apiSecret + '&refresh_token=' + this.authData.refresh_token;
        var headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
        headers = headers.set('Authorization', 'Bearer ' + this.authData.refresh_token);
        return this.http.post(this.config.apiOauthUrl + 'token/', data, { headers: headers }).pipe(catchError(function (e) {
            if (e.status === 401) {
                _this.logout();
                return EMPTY;
            }
            return throwError(e);
        }), map(function (res) {
            _this.authData = res;
            _this.token = _this.authData.access_token;
            _this.isAuthenticated = true;
            _this.authenticatedChanged.next(true);
            _this.authData.expiration = new Date().getTime() + (_this.authData.expires_in * 1000);
            window.localStorage.setItem('auth_data', JSON.stringify(_this.authData));
            return _this.authData;
        }));
    };
    AuthService.prototype.getToken = function () {
        var _this = this;
        return Observable.create(function (observer) {
            if (_this.token != null) {
                if (!_this.authData.expiration || _this.authData.expiration <= new Date().getTime() + 100000) {
                    _this.refresh_token().subscribe(function () {
                        observer.next(_this.token);
                        observer.complete();
                    });
                }
                else {
                    observer.next(_this.token);
                    observer.complete();
                }
            }
            else {
                observer.next(null);
                observer.complete();
            }
        });
    };
    AuthService.prototype.request = function (method, url, data, headers) {
        var _this = this;
        if (data === void 0) { data = null; }
        if (headers === void 0) { headers = new HttpHeaders(); }
        var hasFile = false;
        if (data !== null) {
            if (!isPlatformServer(this.platformId)) {
                var formData = new FormData();
                hasFile = assignFormdata(formData, data);
                if (!hasFile) {
                    data = JSON.stringify(data);
                }
                else {
                    data = formData;
                }
            }
            else {
                data = JSON.stringify(data);
            }
        }
        return Observable.create(function (obs) {
            _this.getToken().subscribe(function () {
                headers = headers.set('Authorization', 'Bearer ' + _this.token);
                if (!hasFile) {
                    headers = headers.set('Content-Type', 'application/json');
                }
                var options = { headers: headers };
                var req;
                if (method === 'POST' || method === 'PUT' || method === 'PATCH') {
                    req = new HttpRequest(method, _this.config.apiUrl + url, data, options);
                }
                else {
                    req = new HttpRequest(method, _this.config.apiUrl + url, options);
                }
                _this.http.request(req).subscribe(function (d2) {
                    if (d2 instanceof HttpResponse) {
                        if (d2.status === 204) {
                            obs.next(null);
                        }
                        else {
                            obs.next(d2.body);
                        }
                    }
                }, function (e) {
                    if (e.status === 401) {
                        _this.logout();
                    }
                    obs.error(e);
                }, function () { return obs.complete(); });
            });
        });
    };
    AuthService.prototype.get = function (url) {
        return this.request('GET', url);
    };
    AuthService.prototype.post = function (url, data) {
        return this.request('POST', url, data);
    };
    AuthService.prototype.patch = function (url, data) {
        return this.request('PATCH', url, data);
    };
    AuthService.prototype.delete = function (url) {
        return this.request('DELETE', url);
    };
    AuthService.decorators = [
        { type: Injectable },
    ];
    AuthService.ctorParameters = function () { return [
        { type: AuthServiceConfig, },
        { type: HttpClient, },
        { type: Object, decorators: [{ type: Inject, args: [PLATFORM_ID,] },] },
    ]; };
    return AuthService;
}());
export { AuthService };
function assignFormdata(formdata, data) {
    var hasFile = false;
    for (var i in data) {
        if (data[i] instanceof File) {
            formdata.append(i, data[i]);
            hasFile = true;
        }
        else if (data[i] instanceof Object) {
            formdata.append(i, JSON.stringify(data[i]));
        }
        else {
            formdata.append(i, data[i]);
        }
    }
    return hasFile;
}
