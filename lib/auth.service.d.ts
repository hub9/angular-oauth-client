import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
export declare class AuthServiceConfig {
    apiId: string;
    apiSecret: string;
    apiUrl: string;
    apiOauthUrl: string;
    unauthorizedRoute: string;
}
export declare class AuthService {
    private config;
    private http;
    private platformId;
    isAuthenticated: boolean;
    token: string;
    me: any;
    private authData;
    authenticatedChanged: Subject<any>;
    constructor(config: AuthServiceConfig, http: HttpClient, platformId: Object);
    login(username: string, password: string): Observable<any>;
    logout(): void;
    refresh_token(): Observable<any>;
    getToken(): Observable<any>;
    request(method: string, url: string, data?: any, headers?: any): Observable<any>;
    get(url: string): Observable<any>;
    post(url: string, data: any): Observable<any>;
    patch(url: string, data: any): Observable<any>;
    delete(url: string): Observable<any>;
}
